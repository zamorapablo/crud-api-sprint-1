# CRUD API Sprint 1

API RESTful creada con Node.js, Express y Swagger para Acámica.

## Descripción
El proyecto es una API CRUD (Create, Read, Update, Delete) que posee registro y login de usuario y les permite realizar pedidos y ver el estado de los mismos; también tiene usuarios administradores que pueden realizar operaciones CRUD de productos y medios de pago disponibles, todo utilizando las tecnologías Node.js (para montar el servidor), Express (para el manejo de requests y responses, además de middlewares) y Swagger (para la documentación de cada endpoint)


## Installation
1. Clonar el repositorio
2. Situarse en la carpeta del repo 
3. Revisar que estén instaladas las dependencias necesarias (express, swagger-jsdoc, swagger-ui-express), podemos instalarlas ejecutando en la terminal: 
`npm i express swagger-jsdoc swagger-ui-express`
4. Para montar el servidor, ejecutar:
`node app`
5. Importar colección de postman para testear endpoints (Sprint 1.postman_collection.json)

## Uso
Para más información sobre los endpoints, visitar http://localost:3000/api-docs con el servidor montado


## Autor
Pablo Zamora

