const express = require("express");
const app = express();
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Sprint 1 mi primera API",
      version: "1.0.0",
    },
  },
  apis: ["./app.js"],
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use((req, res, next) => {
  console.log(`URL:${req.url}  method:${req.method}`);
  next();
});

const users = [
  {
    username: "admin",
    nombre: "administrador administrativo",
    email: "admin@admin.com",
    telefono: "0-800-404",
    direccion: "Casa del admin",
    pass: "admin",
    id: 0,
    admin: true,
    estado: false,
  },
  {
    username: "testuser1",
    nombre: "Tester 1",
    email: "test@testing.com",
    telefono: "123456",
    direccion: "San Martin 123",
    pass: "pass",
    id: 1,
    admin: false,
    estado: false,
  },
];

const productos = [
  {
    id: 0,
    nombre: "Burger",
    precio: 500,
  },
  {
    id: 1,
    nombre: "Lomito",
    precio: 300,
  },
  {
    id: 2,
    nombre: "Docena de empanadas",
    precio: 333,
  },
];
const pedidos = [
  {
    id: 0,
    userid: 1,
    productos: [
      {
        id: 1,
        cantidad: 1,
        total: 500,
      },
      {
        id: 2,
        cantidad: 1,
        total: 300,
      },
    ],
    direccion: "Mi calle 1024",
    estado: "Pendiente", //estados posibles: Pendiente, Confirmado, En preparación, Enviado, Entregado
    id_mediodepago: 0,
    nombre_mediodepago: "Efectivo",
  },
];
mediosdepago = [
  {
    id: 0,
    nombre: "Efectivo",
  },
  {
    id:1,
    nombre: "Débito"
  }
];

const esAdmin = (req, res, next) => {
  let userid = parseInt(req.params.userid);
  if (userid != undefined && userid >= 0) {
    if (users[userid].id == userid) {
      if (users[userid].admin == true) {
        next();
      } else {
        res.json("El usuario no es administrador, acceso denegado");
      }
    } else {
      res.json("id inválido");
    }
  } else {
    res.json("El usuario no está logeado");
  }
};

const estaLogeado = (req, res, next) => {
  let userid = parseInt(req.params.userid);
  if (userid === undefined || userid < 0) {
    res.json("Envíe una id válida por parámetro");
  } else {
    let index = users.findIndex((item) => item.id == userid);
    if (index >= 0) {
      if (users[index].id == userid && users[index].estado) {
        next();
      } else {
        res.json("No está logeado");
      }
    } else {
      res.json("id del usuario inválida");
    }
  }
};
const validarPedidoPost = (req, res, next) => {
  const { productos_pedido = false, direccion = false } = req.body;
  res.locals.userid = parseInt(req.params.userid);
  res.locals.userindex = users.findIndex(
    (item) => item.id == res.locals.userid
  );
  if (res.locals.userindex >= 0) {
    res.locals.direccion = direccion
      ? direccion
      : users[res.locals.userindex].direccion;
    res.locals.productosPedido = [];
    if (productos_pedido) {
      let flagCantidad = false;
      let flagID = false;
      for (const item of productos_pedido) {
        let id = item.producto_id;
        let cantidad = item.producto_cantidad;
        if (!cantidad || cantidad <= 0) {
          flagCantidad = true;
          console.log(item);
        } else {
          let indexProd = productos.findIndex((elem) => elem.id == id);
          if (indexProd == -1) {
            flagID = true;
          } else {
            let precio = productos[indexProd].precio * cantidad;
            let producto = {
              id: id,
              cantidad: cantidad,
              precio: precio,
            };
            res.locals.productosPedido.push(producto);
          }
        }
      }
      if (flagCantidad) {
        res.send("Valor de cantidad de un producto erróneo");
      } else if (flagID) {
        res.send("Valor de ID de un producto erróneo");
      } else {
        next();
      }
    } else {
      res.send("Envíe objeto de productos_pedido");
    }
  } else {
    res.send("ID del usuario inválida");
  }
};
/**
 * @swagger
 * /registro:
 *  post:
 *    description: Crea un usuario y guarda sus credenciales en un array
 *    parameters:
 *    - name: username
 *      description: Identificador (nombre) del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: nombre
 *      description: Nombre y apellido del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: telefono
 *      description: Teléfono del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: direccion
 *      description: Dirección del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Contraseña del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.post("/registro", (req, res) => {
  const { username, nombre, email, telefono, direccion, pass } = req.body;
  if (
    username != "" &&
    nombre != "" &&
    email != "" &&
    telefono != "" &&
    direccion != "" &&
    pass != ""
  ) {
    let index = -1;
    users.forEach((user, i) => {
      if (user.email == email) {
        index = i;
      }
    });
    if (index != -1) {
      console.log("Error, ya existe un usuario con este email");
      res.json("Ya existe un usuario con este email");
    } else {
      let usuario = {
        id: users.length,
        email: email,
        pass: pass,
        admin: false,
        estado: false,
      };
      users.push(usuario);
      res.json(`usuario creado con email: ${email}`);
    }
  } else {
    res.json("Email y contraseña son campos requeridos");
  }
});
/**
 * @swagger
 * /login:
 *  post:
 *    description: Logea al usuario y devuelve la id
 *    parameters:
 *    - name: email
 *      description: Email del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    - name: pass
 *      description: Contraseña del usuario
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.post("/login", (req, res) => {
  //logeo
  const { email, pass } = req.body;
  let index = -1;
  users.forEach((user, i) => {
    if (user.email == email) {
      if (user.pass == pass) {
        index = i;
      }
    }
  });
  if (index == -1) {
    console.log(
      `Las credenciales no son válidas, email:${email}, pass:${pass}`
    );
    res.json("Login fallido");
  } else {
    console.log("Login correcto, id: " + users[index].id);
    users[index].estado = true;
    res.json(users[index].id);
  }
});
/**
 * @swagger
 * /productos/{userid}:
 *  get:
 *    description: Devuelve listado de productos, requiere usuario logeado
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.get("/productos/:userid", estaLogeado, (req, res) => {
  res.json(productos);
});
/**
 * @swagger
 * /pedidos/{userid}:
 *  get:
 *    description: Devuelve listado de pedidos del usuario
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.get("/pedidos/:userid", estaLogeado, (req, res) => {
  let userid = req.params.userid;
  let pedidosUser = pedidos.filter((item) => item.userid == userid);
  if (pedidosUser.length == 0) {
    res.json("No realizó pedidos");
  } else {
    res.json(pedidosUser);
  }
});
/**
 * @swagger
 * /pedidos/{userid}:
 *  post:
 *    description: Postea pedido a nombre del usuario
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    - name: productos_pedido
 *      required: true
 *      description: Array con objetos formados por producto_id(int) y producto_cantidad(int)
 *      type: array
 *      items:
 *        type: object
 *        properties:
 *          producto_id:
 *            type: integer
 *          producto_cantidad:
 *            type: integer
 *      in: formData
 *    - name: direccion
 *      description: direccion a ser entregado
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 *
 */

app.post("/pedidos/:userid", [estaLogeado, validarPedidoPost], (req, res) => {
  //buscar como pasar arrays
  let pedido = {
    id: pedidos[pedidos.length - 1].id + 1,
    userid: res.locals.userid,
    productos: res.locals.productosPedido,
    direccion: res.locals.direccion,
    estado: "Pendiente",
    id_mediodepago: undefined,
    nombre_mediodepago: "N/A",
  };
  pedidos.push(pedido);
  res.json("nuevo pedido creado");
});
/**
 * @swagger
 * /pedidos/{userid}:
 *  put:
 *    description: Modifica un pedido del usuario. Enviar id_pedido e id_mediodepago cierra el pedido, simulando que pagó por todos los productos del carrito.
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *
 *    - name: id_pedido
 *      description: id del pedido a editar
 *      in: formData
 *      required: true
 *      type: integer
 *
 *    - name: id_producto
 *      description: id del producto a editar
 *      in: formData
 *      required: false
 *      type: integer
 *
 *    - name: id_producto_nuevo
 *      description: nueva id del producto a modificar
 *      in: formData
 *      required: false
 *      type: integer
 *
 *
 *    - name: cantidad_producto
 *      description: nueva cantidad del producto a editar
 *      in: formData
 *      required: false
 *      type: integer
 *
 *    - name: direccion
 *      description: nueva dirección del pedido
 *      in: formData
 *      required: false
 *      type: string
 *
 *    - name: id_mediodepago
 *      description: id del medio de pago a utilizar
 *      in: formData
 *      required: false
 *      type: string
 *
 *
 *
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.put("/pedidos/:userid", estaLogeado, (req, res) => {
  const {
    id_pedido = false,
    id_producto = false,
    id_producto_nuevo = false,
    cantidad_producto = false,
    direccion = false,
    id_mediodepago = false,
  } = req.body;
  let userid = parseInt(req.params.userid);
  let indice_pedido = pedidos.findIndex((item) => item.id == id_pedido);
  let indice_producto = productos.findIndex((item) => item.id == id_producto);
  if (indice_pedido == -1) {
    res.json("id de pedido inválida");
  } else {
    if (indice_pruducto != -1 || id_mediodepago) {
      if (pedidos[indice_pedido].id != userid) {
        res.json("id inválida, el pedido no corresponde al mismo usuario");
      }
      if (
        pedidos[indice_pedido].estado == "Pendiente" &&
        pedidos[indice_pedido].userid === userid
      ) {
        if (!id_producto && id_mediodepago) {
          index_mediodepago = mediosdepago.findIndex(
            (item) => item.id == id_mediodepago
          );
          if (index_mediodepago != -1) {
            pedidos[indice_pedido].id_mediodepago = id_mediodepago;
            pedidos[indice_pedido].nombre_mediodepago =
              mediosdepago[indice_mediodepago].nombre;
            pedidos[indice_pedido].estado = "Confirmado";
          } else {
            res.json("id de medio de pago inválida");
          }
        } else {
          res.locals.id_producto = id_producto_nuevo
            ? id_producto_nuevo
            : pedidos[indice_pedido].productos[indice_producto].id;
          res.locals.cantidad = cantidad_producto
            ? cantidad_producto
            : pedidos[indice_pedido].productos[indice_producto].cantidad;
          pedidos[indice_pedido].productos[indice_producto].id =
            res.locals.id_producto;
          pedidos[indice_pedido].productos[indice_producto].cantidad =
            res.locals.cantidad;
          if (id_producto || cantidad_producto) {
            pedidos[indice_pedido].productos[indice_producto].total =
              res.locals.cantidad * productos[res.locals.id_producto].precio;
          }
          if (direccion !== "" && direccion) {
            pedidos[indice_pedido].direccion = direccion;
          }
          res.json("Pedido editado con éxito");
        }
      } else {
        res.json("No se pudo editar el pedido, revise documentación");
      }
    } else {
      res.json("La id de producto no corresponde con un producto existente");
    }
  }
});
/**
 * @swagger
 * /admin/pedidos/{userid}:
 *  get:
 *    description: Devuelve listado de pedidos de todos los usuarios, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.get("/admin/pedidos/:userid", esAdmin, (req, res) => {
  res.json(pedidos);
});
/**
 * @swagger
 * /admin/pedidos/{userid}:
 *  put:
 *    description: Modifica el estado de un pedido, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    - name: id_pedido
 *      description: id del pedido a modificar
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: estado
 *      description: estado a asignar
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.put("/admin/pedidos/:userid", esAdmin, (req, res) => {
  //estados posibles: Pendiente, Confirmado, En preparación, Enviado, Entregado
  const { id_pedido = false, estado = false } = req.body;
  if (id_pedido && estado) {
    let index = pedidos.findIndex((item) => item.id == id);
    if (index == -1) {
      res.json("id inválida");
    } else {
      if (
        estado == "Pendiente" ||
        estado == "Confirmado" ||
        estado == "En preparación" ||
        estado == "Enviado" ||
        estado == "Entregado"
      ) {
        pedidos[index].estado = estado; //buscar indice del pedido
        res.json("cambio de estado ok");
      } else {
        res.json(
          "Ingrese un estado válido(Pendiente, Confirmado, En preparación, Enviado o Entregado)"
        );
      }
    }
  } else {
    res.json("Envíe todos los campos requeridos");
  }
});
/**
 * @swagger
 * /mediosdepago/{userid}:
 *  get:
 *    description: Retorna listado de medios de pago
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.get("/mediosdepago/:userid", estaLogeado, (req, res) => {
  res.json(mediosdepago);
});
/**
 * @swagger
 * /admin/mediosdepago/{userid}:
 *  post:
 *    description: Agrega medio de pago nuevo, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    - name: nombre
 *      description: nombre del nuevo medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.post("/admin/mediosdepago/:userid", esAdmin, (req, res) => {
  const { nombre = false } = req.body;
  if (nombre) {
    let mediodepago = {
      id: mediosdepago[mediosdepago.length - 1].id + 1,
      nombre: nombre,
    };
    mediosdepago.push(mediodepago);
    res.json("Nuevo medio de pago creado");
  } else {
    res.json("Revise campos requeridos");
  }
});
/**
 * @swagger
 * /admin/mediosdepago/{userid}:
 *  put:
 *    description: Modifica medio de pago existente, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    - name: id
 *      description: id del medio de pago a modificar
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: nombre
 *      description: nombre del nuevo medio de pago
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.put("/admin/mediosdepago/:userid", esAdmin, (req, res) => {
  const { id = false, nombre = false } = req.body;
  if (nombre && id) {
    let index = mediosdepago.findIndex((item) => item.id == id);
    if (index == -1) {
      res.json("id inválida");
    } else {
      mediosdepago[index].nombre = nombre;
      res.json("Actualización de medio de pago realizado");
    }
  } else {
    res.json(
      "Actualización de medio de pago fallida, revise campos requeridos"
    );
  }
});
/**
 * @swagger
 * /admin/mediosdepago/{userid}:
 *  delete:
 *    description: Elimina medio de pago existente, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    - name: id
 *      description: id del medio de pago a eliminar
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.delete("/admin/mediosdepago/:userid", esAdmin, (req, res) => {
  const { id } = req.body;
  let index = mediosdepago.findIndex((item) => item.id == id);
  if (index == -1) {
    res.json("id inválida");
  } else {
    mediosdepago.splice(index);
    res.json("Medio de pago eliminado.");
  }
});
/**
 * @swagger
 * /admin/productos/{userid}:
 *  post:
 *    description: Agrega producto nuevo, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *    - name: nombre
 *      type: string
 *      required: true
 *      in: formData
 *      description: nombre del nuevo producto
 *
 *    - name: precio
 *      description: precio del nuevo producto
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.post("/admin/productos/:userid", esAdmin, (req, res) => {
  const { nombre = false, precio = false } = req.body;
  if (!nombre || !precio) {
    res.json("Revise campos requeridos");
  } else {
    let producto = {
      id: productos[productos.length - 1].id + 1,
      nombre: nombre,
      precio: precio,
    };
    productos.push(producto);
    res.json("Nuevo producto añadido");
  }
});
/**
 * @swagger
 * /admin/productos/{userid}:
 *  put:
 *    description: Modifica producto existente, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: id numerico del usuario
 *
 *    - name: id
 *      description: id del producto a modificar
 *      in: formData
 *      required: true
 *      type: integer
 *
 *    - name: nombre
 *      description: nuevo nombre del producto
 *      in: formData
 *      required: false
 *      type: string
 *
 *    - name: precio
 *      description: nuevo precio del producto
 *      in: formData
 *      required: false
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.put("/admin/productos/:userid", esAdmin, (req, res) => {
  const { id = false, nombre = false, precio = false } = req.body;
  if (!id) {
    res.json("Revise campos requeridos");
  } else {
    let index = productos.findIndex((item) => item.id == id);
    if (index == -1) {
      res.json("id inválida");
    } else {
      let nombreNuevo = nombre ? nombre : productos[index].nombre;
      let precioNuevo = precio ? precio : productos[index].precio;
      productos[index].nombre = nombreNuevo;
      productos[index].precio = precioNuevo;
    }
    res.json("Se actualizó el producto");
  }
});
/**
 * @swagger
 * /admin/productos/{userid}:
 *  delete:
 *    description: Elimina producto existente, requiere permisos de administrador
 *    parameters:
 *    - in: path
 *      name: userid
 *      schema:
 *        type: integer
 *        minimum: 0
 *      required: true
 *      description: ID numerico del usuario
 *    - name: id
 *      description: ID del producto a eliminar
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 *
 */
app.delete("/admin/productos/:userid", esAdmin, (req, res) => {
  const { id = false } = req.body;
  if (id) {
    let index = productos.findIndex((item) => item.id == id);
    if (index == -1) {
      res.json("id inválida");
    } else {
      productos.splice(index);
      res.json("Operación exitosa");
    }
  } else {
    res.json("El campo id es requerido");
  }
});

app.listen(3000, (req, res) => {
  console.log("Escuchando el puerto 3000");
});
